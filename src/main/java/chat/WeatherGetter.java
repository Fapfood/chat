package chat;

import com.google.gson.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Date;


/**
 * Created by FAPFOOD on 2017-01-23.
 */
public class WeatherGetter {
    private final long WEATHER_DURATION = 1000 * 60 * 10;
    private final String URL = "http://api.openweathermap.org/data/2.5/weather?q=Krakow&appid=27c54840470d0a9e9308acd3938ee967&units=metric&lang=pl";
    private String weather;
    private Date weatherDate;

    public WeatherGetter() {
        this.getCurrentWeather();
    }

    public String getWeather() {
        if (new Date().getTime() - this.weatherDate.getTime() > this.WEATHER_DURATION) {
            this.getCurrentWeather();
        }
        return this.weather;
    }

    private void getCurrentWeather() {
        try {
            JsonObject page = this.getJsonFromUrl(URL);
            int temp = page.get("main").getAsJsonObject()
                    .get("temp").getAsInt();
            String description = page.get("weather").getAsJsonArray()
                    .get(0).getAsJsonObject()
                    .get("description").getAsString();
            this.weather = "W Krakowie jest "
                    + temp + "\u00b0 Celsjusza i jest "
                    + description.toLowerCase() + ".";
        } catch (Exception e) {
            e.printStackTrace();
            this.weather = "Error with getting weather from URL";
        }
        this.weatherDate = new Date();

    }

    private JsonObject getJsonFromUrl(String urlString) throws IOException {
        BufferedReader reader = null;
        try {
            URL url = new URL(urlString);
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
            StringBuffer buffer = new StringBuffer();
            int read;
            char[] chars = new char[4096];
            while ((read = reader.read(chars)) != -1)
                buffer.append(chars, 0, read);

            return (JsonObject) new JsonParser().parse(buffer.toString());
        } finally {
            if (reader != null)
                reader.close();
        }
    }
}
