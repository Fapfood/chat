package chat;

import chat.channels.ChatBotChannel;
import chat.channels.DefaultChannel;
import chat.channels.IChannel;
import chat.channels.UserChannel;
import org.eclipse.jetty.websocket.api.Session;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static spark.Spark.get;


/**
 * Created by FAPFOOD on 2017-01-20.
 */
public class Chat {
    private final Map<String, IChannel> channels = new ConcurrentHashMap<>();
    private final CookieHandler cookieHandle = new CookieHandler();

    public Chat() {
        initializeDefaultChannels();
        initializeRestApi();
    }

    public void addSession(Session session) {
        String username = this.cookieHandle.getUsername(session.getUpgradeRequest().getCookies());
        String channelName = this.cookieHandle.getChannelName(session.getUpgradeRequest().getCookies());
        if (username.isEmpty() || channelName.isEmpty()) {
            session.close();
        } else {
            User user = new User(username);
            if (!this.channels.containsKey(channelName)) {
                this.channels.put(channelName, new UserChannel(channelName));
            }
            this.channels.get(channelName).addUser(session, user);
        }
    }

    public void removeSession(Session session) {
        this.channels.values().stream()
                .filter(iChannel -> iChannel.userExist(session))
                .forEach(iChannel -> {
                            iChannel.removeUser(session);
                            if (iChannel.canDelete()) {
                                this.channels.remove(iChannel.getChannelName());
                            }
                        }
                );
    }

    public void onMessage(Session session, String message) {
        this.channels.values().stream()
                .filter(iChannel -> iChannel.userExist(session))
                .forEach(iChannel -> iChannel.broadcastMessage(session, message));
    }

    private void initializeDefaultChannels() {
        channels.put("Default", new DefaultChannel("Default"));
        channels.put("ChatBot", new ChatBotChannel("ChatBot"));
    }

    private void initializeRestApi() {
        get("/channels", (req, res) -> getJsonChannelsList());
    }

    private String getJsonChannelsList() {
        try {
            return String.valueOf(new JSONObject().put("channelList", channels.values()));
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }
}
