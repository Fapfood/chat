package chat.channels;

import chat.DaysOfWeek;
import chat.Hours;
import chat.User;
import chat.WeatherGetter;
import org.eclipse.jetty.websocket.api.Session;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;


/**
 * Created by FAPFOOD on 2017-01-23.
 */
public class ChatBotChannel extends AbstractChannel {
    private WeatherGetter weatherGetter = new WeatherGetter();

    public ChatBotChannel(String channelName) {
        super(channelName);
    }

    @Override
    public void broadcastMessage(Session session, String message) {
        User user = this.findUser(session);
        this.broadcastMessage(user.getUsername(), session, message);
        this.broadcastBotMessege(session, chatBotAnswer(message));
    }

    @Override
    public void addUser(Session session, User user) {
        super.addUser(session, user);
        this.broadcastBotMessege(session, this.chatBotAnswer("dtghks"));
    }

    @Override
    public boolean canDelete() {
        return false;
    }

    private void broadcastMessage(String sender, Session session, String message) {
        User user = this.findUser(session);
        try {
            session.getRemote().sendString(String.valueOf(new JSONObject()
                    .put("userMessage", this.createHtmlMessageFromSender(sender, message))
                    .put("userlist", new String[]{user.getUsername()}))
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String chatBotAnswer(String message) {
        switch (message.trim()) {
            case "dtghks":
                return "Hej, jestem botem :)";
            case "Hej":
            case "hej":
                return "Hej :)";
            case "Która godzina?":
            case "Która godzina":
            case "która godzina?":
            case "która godzina":
                return this.getHour();
            case "Jaki dziś dzień tygodnia?":
                return this.getDay();
            case "Jaka jest pogoda w Krakowie?":
                return this.weatherGetter.getWeather();
            case "Help":
            case "help":
                return "Available options :\n" +
                        " 'Hej',\n" +
                        " 'Która godzina?',\n" +
                        " 'Jaki dziś dzień tygodnia?',\n" +
                        " 'Jaka jest pogoda w Krakowie?'";
            default:
                return "Unknown command, write 'Help' for avaiable options";
        }
    }

    private String getHour() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH");
        return "Jest " + Hours.toString(Hours.toEnum(Integer.parseInt(sdf.format(cal.getTime())))) + ".";
    }

    private String getDay() {
        Calendar cal = Calendar.getInstance();
        String day = DaysOfWeek.toString(DaysOfWeek.toEnum(cal.get(Calendar.DAY_OF_WEEK) - 1));
        return "Dziś jest " + day + ".";
    }

    private void broadcastBotMessege(Session session, String message) {
        this.broadcastMessage(this.getChannelName(), session, message);
    }
}
