import chat.WebSocketHandler;
import static spark.Spark.*;


/**
 * Created by FAPFOOD on 2017-01-20.
 */
public class Main {
    public static void main(String[] args) {
        staticFiles.location("/public");
        staticFiles.expireTime(600);
        webSocket("/chat", WebSocketHandler.class);
        init();
    }

}