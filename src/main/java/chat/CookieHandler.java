package chat;

import java.net.HttpCookie;
import java.util.List;


/**
 * Created by FAPFOOD on 2017-01-20.
 */
public class CookieHandler {

    public String getUsername(List<HttpCookie> cookies) {
        return this.getValue(cookies, "username");
    }

    public String getChannelName(List<HttpCookie> cookies) {
        return this.getValue(cookies, "channelName");
    }

    public String getValue(List<HttpCookie> cookies, String name) {
        for (HttpCookie cookie : cookies) {
            if (cookie.getName().equals(name)) {
                return cookie.getValue();
            }
        }
        return "";
    }
}
