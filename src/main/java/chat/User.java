package chat;


/**
 * Created by FAPFOOD on 2017-01-20.
 */
public class User {
    private String username;

    public User(String username) {
        this.username = username;
    }

    public String getUsername() {
        return this.toString();
    }

    @Override
    public String toString() {
        return this.username;
    }
}
