package chat;


/**
 * Created by FAPFOOD on 2017-01-23.
 */
public enum DaysOfWeek {
    PN, WT, ŚR, CZ, PT, SB, ND;

    public static String toString(DaysOfWeek day) {
        switch (day) {
            case PN:
                return "poniedzialek";
            case WT:
                return "wtorek";
            case ŚR:
                return "środa";
            case CZ:
                return "czwartek";
            case PT:
                return "piątek";
            case SB:
                return "sobota";
            case ND:
                return "niedziela";
            default:
                return "";
        }
    }

    public static DaysOfWeek toEnum(String day) {
        switch (day) {
            case "poniedzialek":
                return DaysOfWeek.PN;
            case "wtorek":
                return DaysOfWeek.WT;
            case "środa":
                return DaysOfWeek.ŚR;
            case "czwartek":
                return DaysOfWeek.CZ;
            case "piątek":
                return DaysOfWeek.PT;
            case "sobota":
                return DaysOfWeek.SB;
            case "niedziela":
                return DaysOfWeek.ND;
            default:
                return null;
        }
    }

    public static DaysOfWeek toEnum(int day) {
        switch (day) {
            case 1:
                return DaysOfWeek.PN;
            case 2:
                return DaysOfWeek.WT;
            case 3:
                return DaysOfWeek.ŚR;
            case 4:
                return DaysOfWeek.CZ;
            case 5:
                return DaysOfWeek.PT;
            case 6:
                return DaysOfWeek.SB;
            case 7:
                return DaysOfWeek.ND;
            default:
                return null;
        }
    }
}
