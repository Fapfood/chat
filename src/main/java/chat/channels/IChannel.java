package chat.channels;

import chat.User;
import org.eclipse.jetty.websocket.api.Session;


/**
 * Created by FAPFOOD on 2017-01-22.
 */
public interface IChannel {
    String getChannelName();

    void addUser(Session session, User user);

    User removeUser(Session session);

    boolean userExist(Session session);

    void broadcastMessage(Session session, String message);

    boolean canDelete();
}
