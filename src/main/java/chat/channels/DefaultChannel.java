package chat.channels;


/**
 * Created by FAPFOOD on 2017-01-20.
 */
public class DefaultChannel extends UserChannel {

    public DefaultChannel(String channelName) {
        super(channelName);
    }

    @Override
    public boolean canDelete() {
        return false;
    }
}
