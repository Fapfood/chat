package chat;


/**
 * Created by FAPFOOD on 2017-01-23.
 */
public enum Hours {
    H1, H2, H3, H4, H5, H6, H7, H8, H9, H10, H11, H12, H13, H14, H15, H16, H17, H18, H19, H20, H21, H22, H23, H24;

    public static String toString(Hours hour) {
        switch (hour) {
            case H1:
                return "pierwsza";
            case H2:
                return "druga";
            case H3:
                return "trzecia";
            case H4:
                return "czwarta";
            case H5:
                return "piąta";
            case H6:
                return "szósta";
            case H7:
                return "siódma";
            case H8:
                return "ósma";
            case H9:
                return "dziewiąta";
            case H10:
                return "dziesiąta";
            case H11:
                return "jedenasta";
            case H12:
                return "dwunasta";
            case H13:
                return "trzynasta";
            case H14:
                return "czternasta";
            case H15:
                return "piętnasta";
            case H16:
                return "szesnasta";
            case H17:
                return "siedemnasta";
            case H18:
                return "osiemnasta";
            case H19:
                return "dziewiętnasta";
            case H20:
                return "dwudziesta";
            case H21:
                return "dwudziesta pierwsza";
            case H22:
                return "dwudziesta druga";
            case H23:
                return "dwudziesta trzecia";
            case H24:
                return "północ";
            default:
                return null;
        }
    }

    public static Hours toEnum(int hour) {
        switch (hour) {
            case 1:
                return Hours.H1;
            case 2:
                return Hours.H2;
            case 3:
                return Hours.H3;
            case 4:
                return Hours.H4;
            case 5:
                return Hours.H5;
            case 6:
                return Hours.H6;
            case 7:
                return Hours.H7;
            case 8:
                return Hours.H8;
            case 9:
                return Hours.H9;
            case 10:
                return Hours.H10;
            case 11:
                return Hours.H11;
            case 12:
                return Hours.H12;
            case 13:
                return Hours.H13;
            case 14:
                return Hours.H14;
            case 15:
                return Hours.H15;
            case 16:
                return Hours.H16;
            case 17:
                return Hours.H17;
            case 18:
                return Hours.H18;
            case 19:
                return Hours.H19;
            case 20:
                return Hours.H20;
            case 21:
                return Hours.H21;
            case 22:
                return Hours.H22;
            case 23:
                return Hours.H23;
            case 24:
                return Hours.H24;
            default:
                return null;
        }
    }
}
